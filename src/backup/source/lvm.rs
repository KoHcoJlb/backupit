use std::{fmt, io};
use std::any::Any;
use std::fmt::Display;
use std::fs::{create_dir_all, File, remove_dir};
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};
use std::process::{Command, Output, Stdio};
use std::str::FromStr;

use serde::Deserialize;
use sys_mount::{Mount, MountFlags, SupportedFilesystems, Unmount, UnmountDrop, UnmountFlags};

use crate::config::FilterConfig;
use crate::error::{Error, Result, ResultExt};
use crate::utils::{Finally, temp_name};

use super::{BackupSource, BackupSourceSnapshot, DirectorySource};

#[derive(Debug)]
struct MountPoint {
    path: PathBuf,
    device: PathBuf
}

fn parse_mount_points() -> Result<Vec<MountPoint>> {
    let f = File::open("/proc/self/mountinfo")?;
    BufReader::new(f).lines()
        .map(|r|
            r.map_err(|e| e.into())
                .and_then(|s| s.parse()))
        .collect()
}

impl FromStr for MountPoint {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self> {
        let split: Vec<&str> = s.split(" ").collect();
        if let &[_, _, _, _, path, _, .., "-", _, dev, _] = split.as_slice() {
            Ok(MountPoint {
                path: path.into(),
                device: dev.into()
            })
        } else {
            Err(Error::new("format error").into())
        }
    }
}

fn find_mount_by_path<'a>(mount_points: &'a [MountPoint], path: &Path) -> &'a MountPoint {
    let mut path_opt = Some(path);
    while let Some(path) = path_opt {
        if let Some(mount_point) = mount_points.iter()
            .find(|p| &p.path == path) {
            return mount_point;
        }
        path_opt = path.parent();
    };
    unimplemented!("mount point not found");
}

#[derive(Debug)]
struct CommandOutput {
    status: Option<i32>,
    stdout: String,
    stderr: String
}

impl From<Output> for CommandOutput {
    fn from(out: Output) -> Self {
        CommandOutput {
            status: out.status.code(),
            stdout: String::from_utf8_lossy(&out.stdout).to_string(),
            stderr: String::from_utf8_lossy(&out.stderr).to_string()
        }
    }
}

#[derive(Debug)]
enum CommandError {
    Exit(CommandOutput),
    System(io::Error)
}

impl From<io::Error> for CommandError {
    fn from(err: io::Error) -> Self {
        CommandError::System(err)
    }
}

impl Display for CommandError {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        use CommandError::*;

        match self {
            System(err) => write!(formatter, "{}", err),
            Exit(out) => {
                let status = if let Some(status) = out.status {
                    format!("{}", status)
                } else {
                    "killed".to_owned()
                };
                write!(formatter, "command failed: status {}\n\tstdout:{}\n\tstderr:{}",
                    status, out.stdout, out.stderr)
            }
        }
    }
}

impl std::error::Error for CommandError {}

type CommandResult<T> = std::result::Result<T, CommandError>;

trait CommandExt {
    fn run(&mut self) -> std::result::Result<CommandOutput, CommandError>;
}

impl CommandExt for Command {
    fn run(&mut self) -> std::result::Result<CommandOutput, CommandError> {
        self.stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .map_err(|e| CommandError::System(e))
            .and_then(|c| {
                let out = c.wait_with_output()?;
                if !out.status.success() {
                    Err(CommandError::Exit(out.into()))
                } else {
                    Ok(out.into())
                }
            })
    }
}

#[derive(Debug, Deserialize, Clone)]
struct LogicalVolume {
    lv_name: String,
    vg_name: String
}

fn lvm_create_snapshot(lv_name: &str, snapshot_name: &str, cow_size: &str) -> CommandResult<()> {
    Command::new("lvcreate")
        .arg("-s")
        .args(&["-n", snapshot_name])
        .args(&["-L", cow_size])
        .arg(lv_name)
        .run()
        .map(|_| ())
}

fn lvm_remove(lv_name: &str) -> CommandResult<()> {
    Command::new("lvremove")
        .arg("-y")
        .arg(lv_name)
        .run()
        .map(|_| ())
}

fn lvs(name: Option<&str>) -> CommandResult<Vec<LogicalVolume>> {
    #[derive(Deserialize)]
    struct ReportInner {
        lv: Vec<LogicalVolume>
    }

    #[derive(Deserialize)]
    struct Report {
        report: Vec<ReportInner>
    }

    let mut cmd = Command::new("lvs");
    cmd.args(&["--reportformat", "json"])
        .args(&["-o", "lv_name,vg_name"]);
    if let Some(name) = name {
        cmd.arg(name);
    }

    let res = cmd.run()?;
    let mut report: Report = serde_json::from_str(&res.stdout).unwrap();
    Ok(report.report.pop().unwrap().lv)
}

#[derive(Debug, Deserialize)]
pub struct LvmSnapshot {
    path: PathBuf,
    snapshot_size: String
}

impl BackupSource for LvmSnapshot {
    fn backup<'src>(&'src self, filter: &Vec<FilterConfig>) -> Result<Box<dyn BackupSourceSnapshot>> {
        let mounts = parse_mount_points().wrap_err("parse_mount_points")?;
        let mount = find_mount_by_path(&mounts, &self.path);

        let lv_name = mount.device.to_str().unwrap();
        let lv = if let Some(lv) = lvs(Some(lv_name)).wrap_err("list lvs")?.pop() {
            lv
        } else {
            return Err(Error::new("path is not on lv").into());
        };

        let temp_dir = temp_name();
        create_dir_all(&temp_dir).wrap_err("create temp_dir")?;
        let remove_temp_dir = {
            let temp_dir = temp_dir.clone();
            Finally::new(move || {
                remove_dir(temp_dir).print_err("remove tmp dir");
            })
        };

        let lvm_snapshot_name = temp_dir.file_name().unwrap().to_str().unwrap();
        lvm_create_snapshot(lv_name, lvm_snapshot_name, &self.snapshot_size).wrap_err("create snapshot")?;

        let lvm_snapshot_name = format!("{}/{}", lv.vg_name, lvm_snapshot_name);
        let remove_lvm_snapshot = {
            let lvm_snapshot_name = lvm_snapshot_name.clone();
            Finally::new(move || {
                lvm_remove(&lvm_snapshot_name).print_err("lvm_remove");
            })
        };

        let unmount = Mount::new(format!("/dev/{}", lvm_snapshot_name), &temp_dir,
            &SupportedFilesystems::new().unwrap(), MountFlags::empty(), None)
            .wrap_err("mount")?
            .into_unmount_drop(UnmountFlags::empty());

        let joined = temp_dir.join(&self.path.strip_prefix(&mount.path)
            .wrap_err("strip prefix")?);
        Ok(Box::new(LvmSourceSnapshot {
            parent_snapshot: DirectorySource(joined).backup(filter)
                .wrap_err("directory snapshot")?,
            _unmount: unmount,
            _remove_lvm_snapshot: Box::new(remove_lvm_snapshot),
            _remove_temp_dir: Box::new(remove_temp_dir)
        }))
    }
}

struct LvmSourceSnapshot {
    parent_snapshot: Box<dyn BackupSourceSnapshot>,
    _unmount: UnmountDrop<Mount>,
    _remove_lvm_snapshot: Box<dyn Any>,
    _remove_temp_dir: Box<dyn Any>
}

impl BackupSourceSnapshot for LvmSourceSnapshot {
    fn basedir(&self) -> &Path {
        self.parent_snapshot.basedir()
    }

    fn files(&self) -> &[PathBuf] {
        self.parent_snapshot.files()
    }
}
