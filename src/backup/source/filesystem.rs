use std::path::{Path, PathBuf};

use serde::Deserialize;

use crate::backup::source::BackupSourceSnapshot;
use crate::config::FilterConfig;
use crate::error::Error;
use crate::error::Result;
use crate::utils::{filter_paths, list_files};

use super::BackupSource;

#[derive(Deserialize, Debug)]
pub struct FileSource(PathBuf);

impl BackupSource for FileSource {
    fn backup(&self, _: &Vec<FilterConfig>) -> Result<Box<dyn BackupSourceSnapshot>> {
        if !self.0.is_file() {
            return Err(Error::new("path not exists or is not file").into());
        }

        Ok(Box::new(FilesystemSourceSnapshot {
            dir: self.0.parent().unwrap().to_owned(),
            files: vec![self.0.clone()]
        }))
    }
}

#[derive(Deserialize, Debug)]
pub struct DirectorySource(pub PathBuf);

impl BackupSource for DirectorySource {
    fn backup(&self, filter: &Vec<FilterConfig>) -> Result<Box<dyn BackupSourceSnapshot>> {
        if !self.0.is_dir() {
            return Err(Error::new("path not exists or is not directory").into());
        }

        Ok(Box::new(FilesystemSourceSnapshot {
            dir: self.0.to_owned(),
            files: filter_paths(&self.0, &list_files(&self.0), filter).into_iter().cloned().collect()
        }))
    }
}

#[derive(Debug)]
struct FilesystemSourceSnapshot {
    dir: PathBuf,
    files: Vec<PathBuf>
}

impl BackupSourceSnapshot for FilesystemSourceSnapshot {
    fn basedir(&self) -> &Path {
        &self.dir
    }

    fn files(&self) -> &[PathBuf] {
        self.files.as_slice()
    }
}
