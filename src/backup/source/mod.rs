use std::fmt;
use std::fmt::Debug;
use std::path::{Path, PathBuf};

use serde::{Deserialize, Deserializer};
use serde::de::{IgnoredAny, MapAccess};

use filesystem::{DirectorySource, FileSource};

use crate::config::FilterConfig;
use crate::error::Result;

mod filesystem;
#[cfg(unix)]
mod lvm;

pub trait BackupSource: Debug + Sync {
    fn backup(&self, filter: &Vec<FilterConfig>) -> Result<Box<dyn BackupSourceSnapshot>>;
}

impl<'de> Deserialize<'de> for Box<dyn BackupSource> {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de> {
        struct Visitor;

        impl<'de> serde::de::Visitor<'de> for Visitor {
            type Value = Box<dyn BackupSource>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("map")
            }

            fn visit_map<A>(self, mut map: A) -> std::result::Result<Self::Value, <A as MapAccess<'de>>::Error>
            where A: MapAccess<'de>, {
                use serde::de::Error;

                let source_type = map.next_key::<&str>()?;
                let source: Box<dyn BackupSource> = match source_type {
                    Some("file") => Box::new(map.next_value::<FileSource>()?),
                    Some("dir") => Box::new(map.next_value::<DirectorySource>()?),
                    #[cfg(unix)]
                    Some("lvm") => Box::new(map.next_value::<lvm::LvmSnapshot>()?),
                    _ => Err(A::Error::custom("unknown source type"))?
                };

                while let Some((IgnoredAny, IgnoredAny)) = map.next_entry()? {}

                Ok(source)
            }
        }

        deserializer.deserialize_map(Visitor)
    }
}

pub trait BackupSourceSnapshot {
    fn basedir(&self) -> &Path;

    fn files(&self) -> &[PathBuf];
}
