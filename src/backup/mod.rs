use std::fs::{File, remove_file};
use std::io::{Read, Write};
use std::process::{Command, Stdio};
use std::sync::atomic::Ordering;
use std::time::Duration;

use chrono::Local;
use regex::Regex;

use result::BackupResult;
use rotate::rotate;

use crate::config::{BackupLocation, Config};
use crate::console::con;
use crate::error::{Error, Result, ResultExt};
use crate::INTERRUPT;
use crate::options::BackupOptions;
use crate::utils::Finally;
use crate::utils::temp_name;

pub mod source;
pub mod result;
mod rotate;

fn compute_archive_name(location: &BackupLocation) -> String {
    return format!("{}-{}.7z", location.name, Local::now().format("%Y_%m_%d-%H_%M_%S"));
}

#[cfg(unix)]
fn automount(config: &Config) -> Result<Option<sys_mount::UnmountDrop<impl sys_mount::Unmount>>> {
    use sys_mount::{Mount, SupportedFilesystems, Unmount, UnmountFlags, MountFlags};

    Ok(if let Some(automount) = &config.backup_dir.automount {
        Some(Mount::new(automount, &config.backup_dir.path,
                        &SupportedFilesystems::new().unwrap(), MountFlags::empty(), None)?
            .into_unmount_drop(UnmountFlags::empty()))
    } else {
        None
    })
}

#[cfg(not(unix))]
fn automount(_: &Config) -> Result<()> {
    Ok(())
}

#[cfg(unix)]
const PROGRESS_DELIMITER: u8 = '\x08' as u8;

#[cfg(not(unix))]
const PROGRESS_DELIMITER: u8 = '\r' as u8;

#[cfg(unix)]
extern "C" {
    fn setpriority(which: i32, who: i32, prio: i32) -> i32;
}

pub fn run_backup(opts: &BackupOptions, location: &BackupLocation) -> Result<BackupResult> {
    let snapshot = location.source.backup(&location.filter)
        .wrap_err("source error")?;

    let list_file_path = temp_name();
    let mut list_file = File::create(&list_file_path).wrap_err("can't create include file")?;
    let _remove_list_file = Finally::new(|| {
        remove_file(&list_file_path).print_err("remove listfile");
    });
    for path in snapshot.files() {
        let path = path.strip_prefix(snapshot.basedir()).unwrap();
        writeln!(list_file, "{}", path.to_str().unwrap()).wrap_err("listfile write")?;
    }
    // Drop list_file so 7z can use it
    drop(list_file);

    let mut progress = con().progress(format!("archiving {}", location.name));

    let archive_path = opts.config.backup_dir.path.join(compute_archive_name(location));
    let _unmount = automount(&opts.config).wrap_err("automount")?;
    let mut child = Command::new("7z")
        .arg("a")
        .arg("-bso2")
        .arg("-bse2")
        .arg("-bsp1")
        .arg("-spf")
        .arg(&format!("-mx={}", location.compression_level.as_ref()
            .unwrap_or(&opts.config.compression_level).0))
        .arg(&archive_path)
        .arg(format!("@{}", list_file_path.to_str().unwrap()))
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .current_dir(snapshot.basedir())
        .spawn()?;

    #[cfg(unix)]
        unsafe { setpriority(0, child.id() as i32, location.nice.unwrap_or(opts.config.nice)) };
    let stdout = child.stdout.as_mut().unwrap();

    let progress_pattern: Regex = Regex::new(r"(\d+)%").unwrap();
    let mut it = stdout.bytes().map(|r| r.unwrap()).peekable();
    loop {
        if INTERRUPT.load(Ordering::Relaxed) {
            child.kill().print_err("kill 7z");
            child.wait().print_err("wait 7z");
            std::thread::sleep(Duration::from_secs(1));
            return Err(Error::new("interrupted").into());
        }

        let it_local = &mut it;
        if it_local.peek().is_none() {
            break;
        }
        let line = String::from_utf8(it_local.skip_while(|b| *b == PROGRESS_DELIMITER)
            .take_while(|b| *b != PROGRESS_DELIMITER).collect()).unwrap();
        let captures = progress_pattern.captures(&line);
        if let Some(captures) = captures {
            let percent = captures.get(1).unwrap();
            progress.update(percent.as_str().parse::<f64>().unwrap() / 100f64);
        }
    }

    let status = child.wait()?;
    if !status.success() {
        let mut data = Vec::new();
        child.stderr.unwrap().read_to_end(&mut data).unwrap();
        return Err(Error::new(&format!("7zip failed\nOutput:\n{}", String::from_utf8(data).unwrap())).into());
    } else {
        progress.update(1f64);
    }
    drop(progress);

    rotate(opts, location).wrap_err("rotation error")?;

    let metadata = archive_path.metadata().wrap_err("get archive info")?;
    Ok(BackupResult { size: metadata.len() })
}
