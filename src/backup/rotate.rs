use std::fs::remove_file;

use crate::config::BackupLocation;
use crate::console::con;
use crate::error::Result;
use crate::options::BackupOptions;

pub fn rotate(opts: &BackupOptions, location: &BackupLocation) -> Result<()> {
    if location.rotate == 0 {
        return Ok(());
    }

    let mut files = opts.config.backup_dir.path.read_dir()?
        .filter_map(|res| res.map(|entry| {
            let file_name = entry.file_name();
            let location_name = file_name.to_str().unwrap().rsplitn(3, "-").last().unwrap();
            if location_name == location.name { Some(entry.path()) } else { None }
        }).transpose())
        .collect::<std::result::Result<Vec<_>, _>>()?;
    files.sort();
    let for_deletion = files
        .into_iter()
        .rev()
        .skip(location.rotate as usize);

    for path in for_deletion {
        if let Err(e) = remove_file(&path) {
            writeln!(con(), "can't remove old backup {:?}: {}", path, e).unwrap();
        }
    }

    Ok(())
}
