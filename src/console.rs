use std::cell::Cell;
use std::fmt;
use std::io::{stdout, Write};
use std::ops::{Deref, DerefMut};

pub struct ConsoleOutput {
    progress: Cell<Option<*mut Progress>>,
    show_progress: Cell<bool>
}

impl ConsoleOutput {
    pub fn progress(&'static self, title: String) -> ProgressRef {
        assert!(self.progress.get().is_none());

        let mut progress = Box::new(Progress { con: self, title, progress: 0f64 });

        if self.show_progress.get() {
            progress.print_progress();
        } else {
            println!("{}", progress.title);
        }
        self.progress.replace(Some((progress.as_mut() as *mut Progress).cast()));

        ProgressRef { progress }
    }

    fn finish_progress(&self) {
        println!();
        self.progress.replace(None);
    }

    pub fn write_fmt(&self, args: fmt::Arguments) -> fmt::Result {
        match self.progress.get() {
            Some(progress) if self.show_progress.get() => {
                print!("\r{}", args);
                unsafe { &*progress }.print_progress();
            }
            _ => print!("{}", args)
        };
        Ok(())
    }

    pub fn show_progress(&self, show: bool) {
        self.show_progress.replace(show);
    }
}

pub fn con() -> &'static ConsoleOutput {
    static INST: ConsoleOutput = ConsoleOutput {
        progress: Cell::new(None),
        show_progress: Cell::new(true)
    };
    return &INST;
}

unsafe impl Sync for ConsoleOutput {}

pub struct Progress {
    con: &'static ConsoleOutput,
    title: String,
    progress: f64
}

impl Progress {
    pub fn update(&mut self, progress: f64) {
        if progress > 1f64 {
            self.progress = 1f64;
        } else if progress < 0f64 {
            self.progress = 0f64;
        } else {
            self.progress = progress;
        }
        if self.con.show_progress.get() {
            self.print_progress();
        }
    }

    fn print_progress(&self) {
        fn progress_bar(progress: f64) -> String {
            let progress = (progress * 50f64) as i32;

            (&[
                "[",
                &(0..progress).map(|_| "=").collect::<String>(),
                ">",
                &(progress..50).map(|_| "-").collect::<String>(),
                "]"
            ]).concat()
        }

        print!("\r{}: {:6.2}% {}", self.title, self.progress * 100f64, progress_bar(self.progress));
        stdout().flush().unwrap();
    }
}

pub struct ProgressRef {
    progress: Box<Progress>
}

impl<'a> Deref for ProgressRef {
    type Target = Progress;

    fn deref(&self) -> &Self::Target {
        self.progress.as_ref()
    }
}

impl<'a> DerefMut for ProgressRef {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.progress.as_mut()
    }
}

impl<'a> Drop for ProgressRef {
    fn drop(&mut self) {
        self.con.finish_progress();
    }
}
