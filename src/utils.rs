use std::env;
use std::path::{Path, PathBuf};

use rand::Rng;
use walkdir::WalkDir;

use crate::config::FilterConfig;

pub struct Finally<T: FnOnce()>(Option<T>);

impl<T: FnOnce()> Finally<T> {
    pub fn new(f: T) -> Self {
        Finally(Some(f))
    }
}

impl<T: FnOnce()> Drop for Finally<T> {
    fn drop(&mut self) {
        self.0.take().unwrap()();
    }
}

pub fn random_string(len: usize) -> String {
    rand::thread_rng()
        .sample_iter(rand::distributions::Alphanumeric)
        .take(len)
        .collect::<String>()
}

pub fn temp_name() -> PathBuf {
    env::temp_dir().join(&["backupit_", &random_string(10)].join(""))
}

pub fn list_files(path: &Path) -> Vec<PathBuf> {
    let mut files = Vec::new();
    for ref entry in WalkDir::new(path) {
        if let Ok(entry) = entry {
            if !entry.file_type().is_dir() {
                files.push(entry.path().into());
            }
        }
    }
    return files;
}

/// `path` must begin with `base_dir`
pub fn filter_path(base_dir: &Path, path: &Path, filter: &Vec<FilterConfig>) -> bool {
    enum FilterResult {
        Include,
        Exclude,
    }
    use FilterResult::*;

    let path = path.strip_prefix(base_dir).unwrap();

    let result = filter.iter()
        .fold(None, |result, filter| {
            let result = result.unwrap_or(if let FilterConfig::Include(_) = filter {
                Exclude
            } else {
                Include
            });
            Some(match filter {
                FilterConfig::Include(pattern) if pattern.matches_path(&path) => Include,
                FilterConfig::Exclude(pattern) if pattern.matches_path(&path) => Exclude,
                _ => result
            })
        })
        .unwrap_or(Include);
    if let Include = result {
        true
    } else {
        false
    }
}

/// `files`'s paths must begin with `base_dir`
pub fn filter_paths<'a>(base_dir: &Path, files: &'a Vec<PathBuf>, filter: &Vec<FilterConfig>)
                        -> Vec<&'a PathBuf> {
    files.iter().filter(|path| filter_path(base_dir, &path, filter)).collect()
}
