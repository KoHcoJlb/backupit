use std::sync::mpsc::{channel, Receiver};
use std::thread;

use chrono::{DateTime, Local};
use itertools::Itertools;
use scoped_thread::ScopedThread;

use crate::backup::run_backup;
use crate::config::BackupLocation;
use crate::console::con;
use crate::error::{ErrorExt, Result, ResultExt};
use crate::options::BackupOptions;

fn worker(opts: &BackupOptions, tasks: Receiver<&BackupLocation>) {
    for location in tasks {
        if let Err(err) = (|| -> Result<()> {
            writeln!(con(), "backup {}", location.name).unwrap();
            match run_backup(opts, location) {
                Ok(result) => {
                    if let Some(notifier) = &opts.config.notify {
                        notifier.success(location, &result).wrap_err("notify error")?;
                    }
                    Ok(())
                },
                Err(err) => Err(err)
            }
        })() {
            let err = err.into();
            if let Some(notifier) = &opts.config.notify {
                if let Err(err) = notifier.error(location, &err).wrap_err("notify error") {
                    writeln!(con(), "{}", err).unwrap();
                }
            }
            writeln!(con(), "{}", err.wrap(&format!("backup error for {}", location.name))).unwrap();
        }
    }
}

pub fn handler(opts: &BackupOptions) -> Result<()> {
    let (tx, rx) = channel::<&BackupLocation>();

    let _worker = ScopedThread::spawn(|| worker(opts, rx));

    loop {
        let now = Local::now();
        let group_by = opts.config.locations.values()
            .into_iter()
            .map(|loc| (loc.schedule.next_after_now(), loc))
            .sorted_by_key(|&(next, _)| next)
            .group_by(|&(next, _)| next);
        let min = group_by
            .into_iter()
            .min_by_key(|&(next, _)| next)
            .unwrap();
        let min: (DateTime<Local>, Vec<&BackupLocation>) = (min.0, min.1.map(|(_, loc)| loc).collect());

        let scheduled_names = min.1.iter().map(|loc| &loc.name)
            .collect::<Vec<_>>();
        writeln!(con(), "schedule {:?} at {}, {} sec left", scheduled_names, min.0,
            (min.0 - now).num_seconds()).unwrap();

        thread::sleep((min.0 - now).to_std().unwrap());

        for location in min.1 {
            tx.send(location).unwrap();
        }
    }
}
