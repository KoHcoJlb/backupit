use crate::backup::run_backup;
use crate::error::{Error, Result, ResultExt};
use crate::options::BackupOptions;

pub fn handler(opts: &BackupOptions) -> Result<()> {
    let location_id = opts.args.subcommand().1.unwrap().value_of("location_id").unwrap();
    let location = opts.config.locations.get(location_id);

    if let Some(location) = location {
        run_backup(opts, location)
            .wrap_err(&format!("backup failed for location '{}'", location.name))?;
    } else {
        return Err(Error::new("location not found").into());
    }

    Ok(())
}
