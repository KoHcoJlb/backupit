use crate::error::{Error, ResultExt};
use crate::options::BackupOptions;

mod backup;
mod scheduler;

pub fn handle_subcommand(opts: &BackupOptions) -> Result<(), Error> {
    let subcommand = opts.args.subcommand();
    match subcommand {
        ("backup", _) => backup::handler(opts),
        ("scheduler", _) => scheduler::handler(opts),
        _ => return Err(Error::new("subcommand not specified"))
    }.wrap_err(&format!("subcommand `{}` failed", subcommand.0))
}
