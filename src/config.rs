use std::collections::HashMap;
use std::fmt;
use std::fs;
use std::path::{Path, PathBuf};

use cron_expr::CronExpr;
use glob::Pattern;
use serde::{Deserialize, Deserializer};
use serde::de::MapAccess;

use crate::backup::source::BackupSource;
use crate::notify::Notifier;

fn deserialize_glob_pattern<'de, D>(deserializer: D) -> Result<Pattern, D::Error> where
    D: Deserializer<'de> {
    Pattern::new(Deserialize::deserialize(deserializer)?)
        .map_err(|err| serde::de::Error::custom(format!("{}", err)))
}

fn deserialize_locations<'de, D>(deserializer: D) -> Result<HashMap<String, BackupLocation>, D::Error>
    where D: Deserializer<'de> {
    let mut locations: HashMap<String, BackupLocation> = Deserialize::deserialize(deserializer)?;
    for (name, location) in &mut locations {
        location.name = name.to_string();
    }
    Ok(locations)
}

fn deserialize_backup_dir<'de, D>(deserializer: D) -> Result<BackupDir, D::Error>
    where D: Deserializer<'de> {
    struct Visitor;

    impl<'de> serde::de::Visitor<'de> for Visitor {
        type Value = BackupDir;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("str or object")
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E> where
            E: serde::de::Error, {
            Ok(BackupDir {
                path: PathBuf::from(v),
                #[cfg(unix)]
                automount: None,
            })
        }

        fn visit_map<A>(self, map: A) -> Result<Self::Value, <A as MapAccess<'de>>::Error> where
            A: MapAccess<'de>, {
            BackupDir::deserialize(serde::de::value::MapAccessDeserializer::new(map))
        }
    }

    deserializer.deserialize_any(Visitor)
}

#[derive(Debug)]
pub struct CompressionLevel(pub i32);

impl Default for CompressionLevel {
    fn default() -> Self {
        CompressionLevel(5)
    }
}

impl<'de> Deserialize<'de> for CompressionLevel {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error> where
        D: Deserializer<'de> {
        struct Visitor;

        impl<'de> serde::de::Visitor<'de> for Visitor {
            type Value = CompressionLevel;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("integer, one of 0,1,3,5,7,9")
            }

            fn visit_i64<E>(self, level: i64) -> Result<Self::Value, E> where
                E: serde::de::Error, {
                match level {
                    0 | 1 | 3 | 5 | 7 | 9 => Ok(CompressionLevel(level as i32)),
                    _ => Err(serde::de::Error::invalid_value(serde::de::Unexpected::Signed(level), &self))
                }
            }
        }

        deserializer.deserialize_i64(Visitor)
    }
}

#[derive(Debug, Deserialize)]
pub enum FilterConfig {
    #[serde(rename = "include", deserialize_with = "deserialize_glob_pattern")]
    Include(Pattern),
    #[serde(rename = "exclude", deserialize_with = "deserialize_glob_pattern")]
    Exclude(Pattern),
}

#[derive(Debug, Deserialize)]
pub struct BackupLocation {
    #[serde(default)]
    pub name: String,
    pub source: Box<dyn BackupSource>,
    #[serde(default)]
    pub rotate: i32,
    pub schedule: CronExpr,
    #[serde(default)]
    pub filter: Vec<FilterConfig>,
    pub compression_level: Option<CompressionLevel>,
    #[cfg(unix)]
    pub nice: Option<i32>,
}

#[derive(Debug, Deserialize)]
pub struct BackupDir {
    pub path: PathBuf,
    #[cfg(unix)]
    pub automount: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    #[serde(deserialize_with = "deserialize_backup_dir")]
    pub backup_dir: BackupDir,
    #[serde(rename = "location", deserialize_with = "deserialize_locations")]
    pub locations: HashMap<String, BackupLocation>,
    pub notify: Option<Box<dyn Notifier>>,
    #[serde(default)]
    pub compression_level: CompressionLevel,
    #[cfg(unix)]
    #[serde(default)]
    pub nice: i32,
}

impl Config {
    pub fn load<T: AsRef<Path>>(path: T) -> Result<Config, Box<dyn std::error::Error>> {
        Ok(toml::from_str(&fs::read_to_string(path)?)?)
    }
}
