use bytesize::ByteSize;
use serde::Deserialize;
use serde::Serialize;

use crate::backup::result::BackupResult;
use crate::config::BackupLocation;
use crate::error::{Result, ResultExt};
use crate::notify::Notifier;

#[derive(Debug, Deserialize)]
pub struct Discord {
    webhook: String
}

#[derive(Serialize)]
struct WebhookParams {
    content: String
}

impl Discord {
    fn send_message(&self, params: WebhookParams) -> Result<()> {
        let client = reqwest::blocking::Client::new();
        client.post(&self.webhook)
            .json(&params)
            .send()
            .wrap_err("http error")?;
        Ok(())
    }
}

impl Notifier for Discord {
    fn success(&self, location: &BackupLocation, backup_result: &BackupResult) -> Result<()> {
        self.send_message(WebhookParams {
            content: format!("Backup {}, size {}", location.name,
                ByteSize(backup_result.size).to_string_as(true))
        })
    }

    fn error(&self, location: &BackupLocation, error: &Box<dyn std::error::Error>) -> Result<()> {
        self.send_message(WebhookParams {
            content: format!("Backup failed for {}\n{}", location.name, error)
        })
    }
}
