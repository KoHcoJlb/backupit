use std::fmt;
use std::fmt::Debug;

use serde::{Deserialize, Deserializer};
use serde::de::Error as _;
use serde::de::IgnoredAny;
use serde::de::MapAccess;

use discord::Discord;

use crate::backup::result::BackupResult;
use crate::config::BackupLocation;
use crate::error::Result;

mod discord;

pub trait Notifier: Debug + Sync {
    fn success(&self, location: &BackupLocation, backup_result: &BackupResult) -> Result<()>;

    fn error(&self, location: &BackupLocation, error: &Box<dyn std::error::Error>) -> Result<()>;
}

impl<'de> Deserialize<'de> for Box<dyn Notifier> {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de> {
        struct Visitor;

        impl<'de> serde::de::Visitor<'de> for Visitor {
            type Value = Box<dyn Notifier>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("object")
            }

            fn visit_map<A>(self, mut map: A) -> std::result::Result<Self::Value, <A as MapAccess<'de>>::Error>
            where
                A: MapAccess<'de>, {
                let key = map.next_key()?.ok_or(A::Error::custom("empty object"))?;
                let notifier = match key {
                    "discord" => Box::new(map.next_value::<Discord>()?),
                    _ => Err(A::Error::custom("unknown notifier type"))?
                };

                while let Some((IgnoredAny, IgnoredAny)) = map.next_entry()? {}

                Ok(notifier)
            }
        }

        deserializer.deserialize_map(Visitor)
    }
}
