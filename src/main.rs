use std::sync::atomic::{AtomicBool, Ordering};

use clap::{App, Arg, SubCommand};

use options::BackupOptions;

use crate::console::con;
use crate::error::ResultExt;

mod commands;
mod config;
mod options;
mod error;
mod backup;
mod console;
mod notify;
mod utils;

static INTERRUPT: AtomicBool = AtomicBool::new(false);

fn main() {
    if let Err(err) = (|| -> std::result::Result<(), Box<dyn std::error::Error>> {
        ctrlc::set_handler(|| {
            INTERRUPT.store(true, Ordering::Relaxed);
        }).wrap_err("ctrlc::set_handler")?;

        let args = App::new("backupit")
            .arg(Arg::with_name("config")
                .short("c")
                .long("config")
                .help("Path to configuration file")
                .value_name("path")
                .default_value("backupit.config.toml"))
            .arg(Arg::with_name("no_progress")
                .short("P")
                .help("Disable progress bar"))
            .subcommand(SubCommand::with_name("backup")
                .arg(Arg::with_name("location_id").required(true)))
            .subcommand(SubCommand::with_name("scheduler"))
            .get_matches();

        con().show_progress(!args.is_present("no_progress"));

        let config = config::Config::load(args.value_of("config").unwrap())
            .wrap_err("configuration error")?;

        let options = BackupOptions {
            args,
            config,
        };

        commands::handle_subcommand(&options)?;

        Ok(())
    })() {
        eprintln!("{}", err);
    }
}
