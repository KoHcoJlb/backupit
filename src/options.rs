use clap::ArgMatches;

use crate::config::Config;

pub struct BackupOptions<'a> {
    pub args: ArgMatches<'a>,
    pub config: Config,
}
